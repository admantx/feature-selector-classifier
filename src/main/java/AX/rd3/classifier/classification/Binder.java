package AX.rd3.classifier.classification;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;

import AX.rd3.classifier.Data;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import AX.rd3.classifier.utils.SimplifierHtml;
import AX.rd3.classifier.utils.ToolBox;

/**Divide them et impera
 * 
 * Class the check every type with stats collected before
 * and get the final result.
 * @author riccardo
 *
 */
public class Binder {

	private String fType;
	
	//LOG4J
	//Logger logger = Logger.getLogger(ToolBox.class);
	
	public Binder(WebPage molliccio, String fType) {

		this.fType = fType;

		//PropertyConfigurator.configure("log4j.properties");
		//logger.setLevel(Level.OFF);

		this.isaContent(molliccio);


	}

	public void isaContent(WebPage molliccio) {

		double txtqt = 0.0;
		double linkqt = 0.0;

		SimplifierHtml simplified = molliccio.getSimplifier();
		
		//must have some text tag to be a content page
		if (simplified.getWhereText().containsKey("text")
				&& simplified.getWhereText().containsKey("container_with_text")) {
			
			if(simplified.getWhereText().get("text")>
				simplified.getWhereText().get("container_with_text")) {
				txtqt = simplified.getWhereText().get("text");
			
		}	else {
			txtqt = simplified.getWhereText().get("container_with_text");
			}


		if(simplified.getWhereText().containsKey("link")) {
			linkqt = simplified.getWhereText().get("link");
			}
		}

		Data.params[14]= String.valueOf(
				BigDecimal.valueOf(txtqt).setScale(4,RoundingMode.HALF_UP));
		Data.params[15]= String.valueOf(
				BigDecimal.valueOf(linkqt).setScale(4,RoundingMode.HALF_UP));
		Data.params[16]= String.valueOf(this.hasForumInside(molliccio));

		Data.type = fType;

	}

	/**Check forum in url and title
	 * 
	 * Forum very often declare itself in title
	 * or url, to be properly seen by browser.
	 * 
	 * @param molliccio
	 * @return
	 */
	public boolean hasForumInside(WebPage molliccio) {

		return molliccio.getTitleTokenized().contains("forum")
				|| molliccio.getTitleTokenized().contains("forums")
				|| molliccio.getUrl().toLowerCase().contains("forum");
	}

}

package AX.rd3.classifier.classification;

import AX.rd3.classifier.Data;
import org.jsoup.nodes.Document;

import java.util.Optional;

public class Classificator {

    public static String classify(String url, String modelFile,
                                  String dataFile, Optional<Document> Doc){

        weka.Model model = new weka.Model(true, modelFile, dataFile);

        try {
            WebPage page;
            if (!Doc.isPresent()) {
                page = new WebPage
                        (url, Optional.empty());
            }
            else {
                page = new WebPage
                        (url, Doc);
            }

            // get the final result (needed to be linked with boilerpipe)
            new Binder(page, "");

            return model.classify(Data.params);
        }
        catch (Exception e){
            return "KO";
        }

    }
}

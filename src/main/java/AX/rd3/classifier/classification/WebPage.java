package AX.rd3.classifier.classification;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

import AX.rd3.classifier.Data;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import AX.rd3.classifier.utils.SimplifierHtml;
import AX.rd3.classifier.utils.TagInfo;
import AX.rd3.classifier.utils.ToolBox;

/**Collect stats from every webpage
 * 
 * @author riccardo
 *
 */
public class WebPage {

	// util tools
	private ToolBox tooling = new ToolBox();
	
	//tag info in a custom class
	private Map<String, TagInfo> tagOverview = new HashMap<String, TagInfo>();
	
	//common info in a web page.
	private String url;
	private Document doc ;
	private List<String> titleTokenized = new ArrayList<String>();
	private List<String> urlTokenized = new ArrayList<String>();
	private int nodesNumb;
	private int nodesBodyNumb;
	private double tagComplexity;
	private boolean hasGoodMeta;
	
	//links that have 1 child and txt inside
	private boolean hasSimpleLink = false;

	//logic view of html
	private SimplifierHtml simplifier;
	//supportive data structs.
	private List<String> removable = Arrays.asList("|", "_", ";", "-","!","?",",", "\"" );
	
	public WebPage(String url, Optional<Document> Doc) throws IOException {

		if (!Doc.isPresent()) {
			this.setDoc(Jsoup.connect(url)
					.userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36")
					.maxBodySize(0)
					.get());
		}

		else {
			this.setDoc(Doc.get());
		}
        
        this.setNodesNumb(this.getDoc().getAllElements().size());
        this.setNodesBodyNumb(this.getDoc().body().getAllElements().size());
        this.setTitleTokenized(doc.title());
		this.setUrl(url);

		//default setting is false
		this.setHasGoodMeta(false);
		
		//simplifier uses the raw html
		this.setSimplifier(new SimplifierHtml(doc.clone()));
		
		//get info from cleaned html
		this.setTagOverview(tagOverview, this.getDoc());

		//setting tag complexity as: #tags / #total tags (minus #root e html)
		double tempComplex = ((double)(this.getTagOverview().size()-2)*10.0
								/(this.getNodesBodyNumb()));
		BigDecimal bd = new BigDecimal(tempComplex).setScale(4, RoundingMode.HALF_UP);		
		this.setTagComplexity(bd.doubleValue());

		this.setHasSimpleLink();
		this.setUrlTokenized(url);

		Data.params[8] = String.valueOf(this.getUrl().length());
		Data.params[9] = String.valueOf(this.getNodesNumb());
		Data.params[10] = String.valueOf(this.getTagComplexity());
		Data.params[11] = String.valueOf(this.isHasGoodMeta());
		Data.params[12] = String.valueOf(this.isHasSimpleLink());
		Data.params[13] = String.valueOf(this.getSimplifier().getMonoPatternSize());

	}

	/**
	 * @return the doc
	 */
	public Document getDoc() {
		return doc;
	}

	/**
	 * Remove or unwrap useless and generic tags.
	 * @param doc the doc to set
	 */
	public void setDoc(Document doc) {

		//not supp. in html5 : applet
		doc.select("script, br, footer,"
				+ "embed, object, applet, nav, noscript,"
				+ "param, rp, style")
					.remove();
		
		//not supp. in html5 : big,font,basefont
		//i tag is used for social link
		//u deprecated
		doc.select("strong, em, b, mark, small,"
				+ "del, ins, sub, sup, big,"
				+ "basefont, font, ul, ol, symbol, blockquote, cite, dfn, header, aside, u, nobr").unwrap();
		
		doc.select("h1,h2,h3,h4,h5,h6").tagName("AXtitle");
				
		this.doc = doc;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	/**
	 * @return the titleTokenized
	 */
	public List<String> getTitleTokenized() {
		return titleTokenized;
	}
	
	/**
	 * Set a list using token's title.
	 * Every token is taken lowerCase.
	 * @param rawTitle the titleTokenized to set
	 */
	public void setTitleTokenized(String rawTitle) {
		
		String[] splitted = rawTitle.toLowerCase().split(" ");
		List<String> cleanTitle = new ArrayList<String>();

		for(String s: splitted) {
			if(!removable.contains(s) && s.length() >= 2) {
				if(removable.contains(s.substring(s.length()-1))) {
					cleanTitle.add(s.substring(0,s.length()-1));
				}
				else {
				cleanTitle.add(s);
				}
			}
		}
		this.titleTokenized = cleanTitle;	
	}

	/**
	 * @return the nodesNumb
	 */
	public int getNodesNumb() {
		return nodesNumb;
	}

	/**
	 * @param nodesNumb the nodesNumb to set
	 */
	public void setNodesNumb(int nodesNumb) {
		this.nodesNumb = nodesNumb;
	}

	/**
	 * @return the simplifier
	 */
	public SimplifierHtml getSimplifier() {
		return simplifier;
	}

	/**
	 * @param simplifier the simplifier to set
	 */
	public void setSimplifier(SimplifierHtml simplifier) {
		this.simplifier = simplifier;
	}

	/**
	 * @return the tagOverview
	 */
	public Map<String, TagInfo> getTagOverview() {
		return tagOverview;
	}

	/**
	 * @param tagOverview the tagOverview to set
	 */
	public void setTagOverview(Map<String, TagInfo> tagOverview, Document doc) {
		
		Elements list = doc.select("*");
		
		for(Element element: list) {
			
			String tag = element.tagName();
			
			//quality control meta <-> title
			if(!this.isHasGoodMeta() && tag.equals("meta") 
				&& 	
				element.attr("content").split(" ").length >= this.getTitleTokenized().size()
				&& this.getTitleTokenized().size() >= 5) {
				
				String[] tokens = element.attr("content").toLowerCase().split(" ");
				
				if(tooling.isTitleRelated(tokens, this.getTitleTokenized(),
						this.getTitleTokenized().size())) {
					this.setHasGoodMeta(true);
				}
			}
			
			if(!tagOverview.containsKey(tag)) {
				tagOverview.put(tag, new TagInfo());
			}
			else {
				tagOverview.get(tag).simpleAdd(0, 1);
			}

			if(element.hasText()) {
				tagOverview.get(tag).simpleAdd(1, element.ownText().length());
			}
			if(element.childNodeSize()==1 && element.ownText().length()>1) {
				tagOverview.get(tag).simpleAdd(2, 1);
			}
		}
		
		this.tagOverview = tagOverview;
	}

	/**
	 * @return the tagComplexity
	 */
	public double getTagComplexity() {
		return tagComplexity;
	}

	/**
	 * @param tagComplexity the tagComplexity to set
	 */
	public void setTagComplexity(double tagComplexity) {
		this.tagComplexity = tagComplexity;
	}

	/**
	 * @return the hasGoodMeta
	 */
	public boolean isHasGoodMeta() {
		return hasGoodMeta;
	}

	/**
	 * @param hasGoodMeta the hasGoodMeta to set
	 */
	public void setHasGoodMeta(boolean hasGoodMeta) {
		this.hasGoodMeta = hasGoodMeta;
	}

	/**
	 * @return the nodesBodyNumb
	 */
	public int getNodesBodyNumb() {
		return nodesBodyNumb;
	}

	/**
	 * @param nodesBodyNumb the nodesBodyNumb to set
	 */
	public void setNodesBodyNumb(int nodesBodyNumb) {
		this.nodesBodyNumb = nodesBodyNumb;
	}

	/**
	 * @return the urlTokenized
	 */
	public List<String> getUrlTokenized() {
		return urlTokenized;
	}

	/**Split the url
	 * @param rawUrl the urlTokenized to set
	 */
	public void setUrlTokenized(String rawUrl) {
		
		String replaced = rawUrl.replaceAll("/", ".").replace("-", ".");
		String[] splitted = replaced.split("\\.");

		List<String> cleanUrl = new ArrayList<String>();

		for(String s: splitted) {
			if(!removable.contains(s) && s.length() >= 2) {
				if(removable.contains(s.substring(s.length()-1))) {
					cleanUrl.add(s.substring(0,s.length()-1));
				}
				else {
					cleanUrl.add(s);
				}
			}
		}
		this.urlTokenized = cleanUrl;	
	}

	/**
	 * @return the hasSimpleLink
	 */
	public boolean isHasSimpleLink() {
		return hasSimpleLink;
	}

	/**Verify if his links are simple
	 * 
	 * Simple link are based on a tag, de facto standard: 
	 * # (cardinality) of <link_tag> children / # overall <link_tag> >= 80%
	 * Asking for links that has few childred in the page
	 *
	 */
	public void setHasSimpleLink() {
		if(this.getTagOverview().containsKey("a")) {
		if((double)this.getTagOverview().get("a").getNumChildren()/
				this.getTagOverview().get("a").getOccurrenceNum()
				>= 0.8) {
			this.hasSimpleLink=true;
			}
		}
	}
}

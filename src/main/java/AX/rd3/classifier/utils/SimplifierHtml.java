package AX.rd3.classifier.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import AX.rd3.classifier.Data;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**Class to simplify the html.
 * Use custom tag to get a more clear struct
 * 
 * @author riccardo
 *
 */
public class SimplifierHtml {

	// see ToolBox to retrieve info about usage
	private BitSet PatternInfo = new BitSet(2);
	
	private double monoPatternSize = 0;
	
	private Document doc;
	private Integer[] logic = new Integer[6];
	private Map<String, Integer> mapInfo = new HashMap<String, Integer>();
	private Map<String, Integer> whereText = new HashMap<String, Integer>();

	//caution
	private Map<ArrayList<String>,Integer> pattern= new HashMap<ArrayList<String>, Integer>();
	
	public SimplifierHtml(Document doc) {
		this.setDoc(this.LogicHtml(doc));
		
		this.setWhereText(this.getDoc());
		
		this.setLogic();

		ToolBox tooling2 = new ToolBox();
		this.setPatternInfo(tooling2.patternComputation(pattern));
		this.setMonoPatternSize(tooling2.getMonoPatternSize());
	}

	/**
	 * @return the doc
	 */
	public Document getDoc() {
		return doc;
	}

	/**
	 * @param doc the doc to set
	 */
	public void setDoc(Document doc) {
		this.doc = doc;
	}
	
	public Map<String, Integer> getMapInfo() {
		return this.mapInfo;
	}

	/**Change the DOM tree
	 * 
	 * Manipulate the DOM tree to get an 'equivalent'
	 * tree that explains the tag's family and page intent.
	 * @param doc
	 * @return
	 */
	public Document LogicHtml(Document doc) {

		doc.select("h1,h2,h3,h4,h5,h6").tagName("TITLE");
		doc.select("a,link").tagName("LINK");
		doc.select("span,p,pre,code,ins, article").tagName("TEXT");
		doc.select("div, section, textarea").tagName("CONTAINER");
		// span moved in text
		doc.select("img, svg, canvas, g, polygon, defs, iframe, figure,"
				+ "clippath,i, picture, source").tagName("GRAPHICS");
		
		doc.select("button, input, option").tagName("USER_INTERACTION");
		doc.select("td, tr, tbody, thead").tagName("TABLE");

		Data.params[0] = String.valueOf((doc.select("TITLE").size()));
		Data.params[1] = String.valueOf(doc.select("LINK").size());
		//table
		Data.params[2] = String.valueOf(0.0);
		Data.params[3] = String.valueOf(doc.select("GRAPHICS").size());
		//user interaction
		Data.params[4] = String.valueOf(0.0);
		Data.params[5] = String.valueOf(doc.select("TABLE").size());
		Data.params[6] = String.valueOf(doc.select("TEXT").size());;

		return doc;
	}

	/**Get the top 3 logic tag's family.
	 * 
	 * Get all the occurence in the logic way (see above)
	 * and return them, but not the occurency number
	 * 
	 * @return the top logic
	 */
	public String[] getTopLogic() {
		
		int step = 0;
		String[] top3 = {"","",""};
		for(int i=0; i<6; i++) {
		for(Map.Entry<String, Integer> cursor : this.mapInfo.entrySet()) {
			if(step<3) {
		if(cursor.getValue().equals(this.logic[i]) && !Arrays.asList(top3).contains(cursor.getKey())) {
			top3[step] = cursor.getKey();
			step += 1;
			break;
					}
				}
			}
		}
		return top3;
	}

	public void setLogic() {
		
		this.logic[0] = doc.select("TITLE").size();
		this.mapInfo.put("title", this.logic[0]);
		
		this.logic[1] = doc.select("LINK").size();
		this.mapInfo.put("link", this.logic[1]);
		
		this.logic[2] = doc.select("TEXT").size();
		this.mapInfo.put("text", this.logic[2]);
		
		this.logic[3] = doc.select("GRAPHICS").size();
		this.mapInfo.put("graphics", this.logic[3]);
		
		this.logic[4] = doc.select("USER_INTERACTION").size();
		this.mapInfo.put("user_interaction", this.logic[4]);
		
		this.logic[5] = doc.select("CONTAINER_WITH_TEXT").size();
		this.mapInfo.put("container_with_text", this.logic[5]);
		
		Arrays.sort(logic,Collections.reverseOrder());
	}
	
	/**Get where text is and 1 approach to the pattern.
	 * 
	 * Size for the patterns is 5 to fit a pretty common patterns in
	 * section page (link-image-title-link-text for example)
	 * 
	 * @param doc
	 */
	public void setWhereText(Document doc) {
		
		Elements list = doc.body().getAllElements();
		//current tag
		String curtag= null;
		ArrayList<String> plist = new ArrayList<String>();
		
		for(Element el: list) {
			
			if(el.tagName().equals("container")) {
			if(el.hasAttr("role")) {
				el.children().tagName("graphics");
				}
			}
			
			if(el.ownText().length()>1) {
				if(el.tagName().equals("container")) {
					el.tagName("CONTAINER_WITH_TEXT");
					}
				
				if(!this.whereText.containsKey(el.tagName())) {
					this.whereText.put(el.tagName(), el.ownText().length());
				}
				else {
					this.whereText.put(el.tagName(), this.whereText.get(el.tagName())
							+el.ownText().length());
				}
			}
			curtag=el.tagName();
			
			if(plist.size()>=5) {
				Collections.sort(plist);
				if(pattern.containsKey(plist)) {
					int a = pattern.get(plist);
					pattern.put(plist,  a+=1);
				}
				else {
					 Collections.sort(plist);
					 pattern.put((ArrayList<String>) plist.clone(), 1);
				}
				plist.clear();
			}
			
			if(!curtag.equals("container") && !curtag.equals("body")) {

				if(!curtag.equals("li")) {
					plist.add(curtag);
				}
				
			}
		} 
	}
	
	public Map<String, Integer> getWhereText() {
		return this.whereText;
	}

	/**
	 * @return the patternInfo
	 */
	public BitSet getPatternInfo() {
		return PatternInfo;
	}

	/**
	 * @param patternInfo the patternInfo to set
	 */
	public void setPatternInfo(BitSet patternInfo) {
		PatternInfo = patternInfo;
	}

	/**
	 * @return the monoPatternSize
	 */
	public double getMonoPatternSize() {
		return monoPatternSize;
	}

	/**
	 * @param d the monoPatternSize to set
	 */
	public void setMonoPatternSize(double d) {
		this.monoPatternSize = d;
	}
}

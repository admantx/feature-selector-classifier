package AX.rd3.classifier.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import AX.rd3.classifier.Data;
//import org.apache.log4j.PropertyConfigurator;

public class ToolBox {
	
	private ArrayList<String> nonvaluable1 =  new ArrayList<String>(Arrays.asList
			("user_interaction", "user_interaction","user_interaction","user_interaction","user_interaction"));
	private ArrayList<String> nonvaluable2 =  new ArrayList<String>(Arrays.asList
			("table", "table","table","table","table"));
	private ArrayList<String> nonvaluable3 =  new ArrayList<String>(Arrays.asList
			("link", "link","link","link","link"));
	private ArrayList<String> special1 =  new ArrayList<String>(Arrays.asList
			("graphics", "graphics","graphics","graphics","graphics"));
	
	/*
	 * polimorphic variable:
	 * i want to know how many candidates are there
	 * how many times a candidate occurs
	 */
	private double monoPatternSize = 0.0;
	
	/* 0: valid pattern
	 * 1: unique pattern
	 * 2: link pattern
	 * 3: graphics pattern
	 */
	private BitSet PatternInfo = new BitSet(4);
	
	//Logger  logger = Logger.getLogger(ToolBox.class);

	public ToolBox() {
		//PropertyConfigurator.configure("log4j.properties");
		//logger.setLevel(Level.OFF);
	};
	
	public boolean isTitleRelated(String[] unknownTokenize, List<String> title, double len) {
		
	double contained = 0.0;
			
		for(String unknownWord: unknownTokenize) {
			if(title.contains(unknownWord)) {
				contained += 1;
			}
		}	
		//dummy treshold
		return contained / len >= 0.5;
	}
	
	/**Pattern computation and bitset setting
	 * 
	 * See the fixed pattern above to better understand which pattern is special 
	 * (discarded or noticed). Can be modified with huge impact on results.
	 * 
	 * @param patterns
	 * @return
	 */
	public BitSet patternComputation(Map<ArrayList<String>,Integer> patterns) {
		
		double total = 0.0;
		boolean is;
		
		// is : HoW Candidates aRe There: "hwcrt"
		double hwcrt = 0.0;
		// pattern numbers if  qt >1
		double goover = 0.0;
		
		PatternInfo.set(0, false);
		PatternInfo.set(1, false);
		PatternInfo.set(2, false);
		PatternInfo.set(3, false);
		
		Iterator<Entry<ArrayList<String>, Integer>> iter = patterns.entrySet().iterator();
		while (iter.hasNext()) {
		    Entry<ArrayList<String>, Integer> entry = iter.next();
		    if(!entry.getKey().equals(special1) && (entry.getValue()==1
		    	||entry.getKey().equals(nonvaluable1) 
		    	||entry.getKey().equals(nonvaluable2))){
		        iter.remove();
		    }
		    else {	
		    	if(entry.getKey().equals(nonvaluable3)) {
		    		total+=entry.getValue()/2;
		    		}
		    	else{total+=entry.getValue();}
		    	}
		}
		if(patterns.containsKey(special1) 
			&& patterns.size()==1) {
			//logger.debug("special1");
			PatternInfo.set(0,true);
			PatternInfo.set(3, true);
			this.setMonoPatternSize(patterns.get(special1));
		}

		// with +0.45 we're asking to be top rounded in most case 
		long part = Math.round(total/patterns.size()+0.45);
		//logger.trace(total+"/"+patterns.size()+"="+part);

		Set<ArrayList<String>> candidateSet = new HashSet<ArrayList<String>>();
		
		if(patterns.size()>0){
			for (Entry<ArrayList<String>, Integer> entry : patterns.entrySet()) {
				if (entry.getValue() > (part + 1)) {
					goover += 1;

					if (this.humanRead(entry.getKey())) {

						hwcrt += 1;
						candidateSet.add(entry.getKey());
					}
				}
			}
		}

		if(candidateSet.size()==1) {
			PatternInfo.set(1,true);
			is=this.humanRead(candidateSet.iterator().next());
		} 
		else is= candidateSet.size() != 0;

		if(candidateSet.size()>1) {
			this.setMonoPatternSize(hwcrt/goover);
		}
		
		else if(candidateSet.size()==1) {
			if(candidateSet.iterator().next().equals(nonvaluable3)) {
				PatternInfo.set(2, true);
			}
			this.setMonoPatternSize(patterns.get(candidateSet.iterator().next()));
		}
		PatternInfo.set(0, is);

		switch (PatternInfo.cardinality()){
			case 0:
				Data.params[7]="VALID";
				break;
			case 1:
				Data.params[7]="UNIQUE";
				break;
			case 2:
				Data.params[7]="LINK";
				break;
			case 3:
				Data.params[7]="GRAPHICS";
				break;
			default:
				Data.params[7]="NULL";
				break;
		}

		Data.pattern = Data.params[7];

		return PatternInfo;
	}
	
	/**Pattern that have some impact in a section page.
	 * 
	 * Pattern that has some significant meaning (for a human):
	 * (link-title-link-text-image for example) an human can image
	 * a sort of structure and he can visualize it too prob.
	 * Can be modified and amplified with huge impact (home-tested)
	 * 
	 * @param key
	 * @return
	 */
	public boolean humanRead(ArrayList<String> key) {
		
		boolean isread = false;
		if(key.contains("graphics") && key.contains("link")){
			if (((Collections.frequency(key, "text")>=1 || Collections.frequency(key, "container_with_text")>=1))){
				isread=true;
				if(key.contains("axtitle")) {
					isread = true;
				}
			}
			else if((Collections.frequency(key, "text")==0 && Collections.frequency(key, "container_with_text")==0)) {
				isread = true;
				if(key.contains("axtitle")) {
					isread = true;
				}
			}
		}
		if(key.equals(nonvaluable3)) { isread = true;	}

		return isread;
	}

	/**
	 * @return the monoPatternSize
	 */
	public double getMonoPatternSize() {
		return monoPatternSize;
	}

	/**
	 * @param d the monoPatternSize to set
	 */
	public void setMonoPatternSize(double d) {
		this.monoPatternSize = d;
	}

}

package AX.rd3.classifier.utils;


/**Custom class for tags.
 * 
 * @author riccardo
 *
 */
public class TagInfo{

	private int occurrenceNum;
	private int txtLength;
	private int numChildren;
	
	
	public TagInfo() {
		this.setOccurrenceNum(1);
		this.setTxtLength(0);
		this.setNumChildren(0);
	}
	
	/**
	 * @return the occurrenceNum
	 */
	public int getOccurrenceNum() {
		return occurrenceNum;
	}

	/**
	 * @param occurrenceNum the occurrenceNum to set
	 */
	public void setOccurrenceNum(int occurrenceNum) {
		this.occurrenceNum = occurrenceNum;
	}
	
	/**
	 * @return the txtLength
	 */
	public int getTxtLength() {
		return txtLength;
	}
	
	/**
	 * @param txtLength the txtLength to set
	 */
	public void setTxtLength(int txtLength) {
		this.txtLength = txtLength;
	}
	
	/**A unique function to compute simple operation
	 * 
	 * Same operation (add) on different fields, 
	 * the splitter is an integer (for the switch)
	 * @param param
	 * @param toadd
	 */
	public void simpleAdd(int param, int toadd) {
		
		switch(param) {
		case 0: {
			this.setOccurrenceNum(this.getOccurrenceNum()+toadd);
			break;
			}
		case 1: {
			this.setTxtLength(this.getTxtLength()+toadd);
			break;
			}
		case 2:{
			this.setNumChildren(this.getNumChildren()+toadd);
			}
		}
	}

	/**
	 * @return the numChildren
	 */
	public int getNumChildren() {
		return numChildren;
	}
	/**
	 * @param numChildren the numChildren to set
	 */
	public void setNumChildren(int numChildren) {
		this.numChildren = numChildren;
	}
}

package AX.rd3.classifier;

import AX.rd3.classifier.classification.Binder;
import AX.rd3.classifier.classification.WebPage;

import java.io.*;
import java.util.Arrays;
import java.util.Optional;

/**Main class.
 * 
 * In the version used to try home testing has
 * a loop and abs. path to read from file.
 *  
 * @author riccardo
 *
 */
public class App 
{

    public static void main(String[] args) throws Exception
    {

        //FileOutputStream fos = new FileOutputStream("/Users/resposti/test-weka/train.arff");
        //DataOutputStream outStream = new DataOutputStream(new BufferedOutputStream(fos));

        FileWriter writer = new FileWriter("/Users/resposti/test-weka/trainxxx.arff");
        BufferedWriter outStream = new BufferedWriter(writer);

		String header = "%TITLE,LINK,CONTAINER,GRAPHICS,USER_INT,TABLE,TEXT,PATTERN," +
				"#NODE,COMPLEXITY,META,SIMPLE-LINK,PATTERN-SIZE,TXT-QT,HASFORUM,HASCOMICS\n";

        outStream.write(header);
        outStream.flush();

		String data = "\n@RELATION webpage\n" +
				"\n" +
				"@ATTRIBUTE titles       NUMERIC\n" +
				"@ATTRIBUTE links        NUMERIC\n" +
				"@ATTRIBUTE containers   NUMERIC\n" +
				"@ATTRIBUTE graphics     NUMERIC\n" +
				"@ATTRIBUTE user_int     NUMERIC\n" +
				"@ATTRIBUTE tables       NUMERIC\n" +
				"@ATTRIBUTE text\t\tNUMERIC\n" +
				"@ATTRIBUTE pattern      {NULL,VALID,UNIQUE,LINK,GRAPHICS}\n" +
				"@ATTRIBUTE url-len       NUMERIC\n" +
				"@ATTRIBUTE nodes\tNUMERIC\n" +
				"@ATTRIBUTE complexity\tNUMERIC\n" +
				"@ATTRIBUTE goodmeta\t{true,false}\n" +
				"@ATTRIBUTE simple-link\t{true,false}\n" +
				"@ATTRIBUTE patternsize\tNUMERIC\n" +
				"@ATTRIBUTE txt-qt\tNUMERIC\n" +
				"@ATTRIBUTE link-qt\tNUMERIC\n" +
				"@ATTRIBUTE hasforum\t{true,false}\n" +
				"@ATTRIBUTE type\t\t{news,forum,gallery,home,comics,directory}\n" +
				"\n" +
				"@DATA\n" +
				"%\n";

        outStream.write(data);
        outStream.flush();

		File folder = new File("/Users/resposti/test-weka/");
		File[] listOfFiles = folder.listFiles();
		BufferedReader br;

		assert listOfFiles != null;

		weka.Model model = new weka.Model(true,"/Users/resposti/j48.model","/Users/resposti/train.arff");
		//model.drawModel();

		for (File file : listOfFiles) {
			FileInputStream fstream =
					new FileInputStream(file);
			 br = new BufferedReader(new InputStreamReader(fstream));
			if (file.isFile()) {

		String url ;


				while ((url = br.readLine()) != null)   {
			try {

				// load the webpage stats (the simplifier is build inside)
				//Optional.of(T)
				Optional.empty();

				WebPage page = new WebPage
						(url, Optional.empty());

				// get the final result (needed to be linked with boilerpipe)
				Binder b = new Binder(page, file.getName().split("\\.")[0]);

				//System.out.println(url);
				//System.out.print(Arrays.toString(Data.params));

                String features = Arrays.toString(Data.params)
                        .replaceAll(" ","")
                        .replaceAll("\\[|]","")
                        +","+Data.type+"\n";

                outStream.write(features);
                outStream.flush();


				//System.out.println( " <----> " + model.classify(Data.params));

			}
			catch(Exception ignored) {
			}
		}
				outStream.close();
		        br.close();
    		}
		}
	}

			}
